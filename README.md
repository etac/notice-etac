# notice-etac

Notice d'utilisation de eTAC en classe.

## Editer la notice
La notice est écrite au format markdown. Vous pouvez l'éditer directement en ligne depuis [l'éditeur web](https://gitlab.inria.fr/etac/notice-etac/blob/master/notice_utilisation_CARDS.md).


## Télécharger le pdf buildé automatiquement
La notice est buildé automatiquement à chaque build.
Vous pouvez la télécharger en allant dans le Téléchargement > Artefacts > PDF

![](img/download_pdf.png)

## Exporter la notice en pdf depuis son ordinateur

### Installer pandoc et LaTeX
La notice est exportée depuis la syntaxe markdown en PDF grace à pandoc et LaTeX.

Sous Windows, avec [chocolatey](https://chocolatey.org/install) 
```bash
choco install -y pandoc miktex
```

Sous OSX avec [homebrew](https://docs.brew.sh/Installation)
```bash
brew install pandoc homebrew/cask/basictex
```

### « Compiler » la notice
```bash
pandoc notice_utilisation_CARDS.md -o notice.pdf
```

