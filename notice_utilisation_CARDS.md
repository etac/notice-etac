---
documentclass: extarticle
papersize: a4
geometry: "margin=1.8cm"
fontsize: 12pt
---

# Notice d'utilisation de CARDS 
*Pour consulter la documentation complète, rendez-vous sur https://frama.link/doc-CARDS*



| ![](img/videoproj.png){ height=250px} |         ![](img/pc.png){ height=250px} |
|:--------------------------------------:|:--------------------------------------|
|*Figure 1 : Vidéo-projecteur*           | *Figure 2 : Allumage du PC*            |

## Démarrer le dispositif

1. Démarrer le vidéo-projecteur en appuyant sur le bouton d'alimentation (Fig. 1). Pensez à enlever le cache transparent sous le vidéoprojecteur s'il est présent.

2. Démarrer le PC sous la table en appuyant sur le bouton (Fig. 2).

|      ![](img/START.png){ height=110px} |   ![](img/ebeam.png){ height=110px}                   |
|:-------------------------------------------:|:------------------------------:|
| *Figure 3 : Dossiers Medias et icône START* | *Figure 4 : calibrer le eBeam* |

3. Copier depuis une clé USB les médias à utiliser dans le dossier `Medias` sur le bureau (Fig. 3). 

4. **Calibrer le eBeam Edge +** en appuyant sur le bouton central de la base (Fig. 4). Une fenêtre va s'ouvrir avec une croix. Cliquez dessus avec le stylet, et répetez l'opération 9 fois jusqu'à la fermeture du programme.

|![](img/ouverture_calib.png){ height=250}| ![](img/alignement.png){ height=250px}|
|:-------------------------------:|:-------------------------------:|
| *Figure 5 : Ouverture de la calibration* | *Figure 6 : Alignement de la feuille* |

4. Démarrer l'application en cliquant sur l'icône `START` sur le bureau. Il vous est demandé si vous souhaitez calibrer la caméra :
  - Si le dispositif a déjà été utilisé précédemment, presser `Entrée` pour **ne pas** le re-calibrer. Si vous observez un écart important entre la projection et les cartes papier, fermez l'application et relancez la calibration.
  - Si vous venez de l'installer dans l'établissement, il faut calibrer la caméra. Pour celà :
    - Presser la touche `Y` puis `Entrée` pour lancer la **calibration de la caméra**.
    - Déplacer la fenêtre de la vue de la caméra afin de voir l'affichage du damier (Fig. 5)
    - Aligner la feuille de calibration avec l'affichage du damier (Fig. 6).
    - Ouvrir la fenêtre de la caméra au premier plan (en appuyant sur la touche Windows), et **presser la touche `t`** (comme "table") pour calibrer.
    - Vous pouvez vérifier que la calibration a été prise en compte avec l'ajout de lignes dans le Terminal
    - Fermer les fenêtres (en commançant par celle du terminal noir) en appuyant sur les touches `Alt+f4`

## Arrêter
      
1. Quitter le logiciel en appuyant sur les touches `Alt+F4`.
2. Afficher la fenêtre d'arrêt du PC en appuyant sur la combinaison de touches `Alt+F4` .

| ![](img/quit.png){ height=150px}|![](img/arret.png){ height=150px}|
|:-----------------------------:|:----------------------------------:|
| *Figure 7 : touches `Alt+F4`* | *Figure 8 : fenêtre d'arrêt du PC* |

3. Sélectionner "Arrêter" dans la fenêtre d'arrêt.
4. Éteindre le vidéo-projecteur en appuyant **deux fois** sur son bouton d'arrêt (Fig. 1).

